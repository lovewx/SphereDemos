package com.java;

/**
 * 理解final的总原则就是：相当于C里的宏变量，编译时就已经确定了
 */
// 1. final修饰类：该类不能被继承
public final class FinalTest {

    // 2. final修饰成员变量，必须直接初始化，或者在构造中初始化。【宏变量必须初始化】
    private final MyEntity entity = new MyEntity();

    // 3. final修饰基本数据类型：值不可变
    public void testBaseDataType(final Integer num){
        //num = 1; 编译报错：值不可变
    }

    // 4. final修改引用类型：值可变，引用不可变
    public void testReferenceType(final MyEntity entity){
        // 值可变
        entity.setAge(10);
        entity.setName("peter");
        //entity = new MyEntity(); 编译就会报错：引用不可变
    }

    // 5. final修饰方法：相当于变成静态方法
    public void TestMethod(String str){
        System.out.println(str);
    }

    // 6. final修饰类时，成员变量并没有隐式指定为final
    private MyEntity entity2 ;
    public MyEntity change(MyEntity entity3){
        entity2 = entity3;
        return entity2;
    }

    public static void main(String[] args) {
        // 验证final的类的成员变量并没有被默认指定为final：因为引用可变
        MyEntity e = new FinalTest().new MyEntity();
        e.setName("abc");
        MyEntity m = new FinalTest().change(e);
        System.out.println(m.getName());
    }


    // 引用类型变量
    public class MyEntity{
        private String name;
        private Integer age;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Integer getAge() {
            return age;
        }

        public void setAge(Integer age) {
            this.age = age;
        }
    }



}
