package com.multiThread;

import java.util.concurrent.*;

/**
 *  Callable里的泛型，是指定返回结果类型的
 *   FutureTask实现了Runnable和Future两个接口，所以它既可以作为Runnable被线程执行，又可以作为Future得到Callable的返回值
 *   核心原理：调用Future的get()方法会阻塞当前线程，直到执行完返回结果后才唤醒
 */
public class CallableTest {

    public static void main(String[] args) throws Exception{

        Callable<Integer> callable = new Callable<Integer>() {
            @Override
            public Integer call() throws Exception {
                return 2;
            }
        };


        // FutureTask可以获取返回值
        FutureTask<Integer> futureTask = new FutureTask(callable);

        // FutureTask实现了Runnable接口，所以在底层源码里FutureTask覆写的run法里调用了Callable的call()方法
        new Thread(futureTask).start();

        // 获取返回值:当前线程阻塞，直到返回结果
        Integer result = futureTask.get();




        // Callable也可以在线程池里提交
        ExecutorService threadPool = Executors.newFixedThreadPool(4);

        Future<Integer> future = threadPool.submit(new Callable<Integer>() {
            public Integer call() throws Exception {
                Thread.sleep(4000);
                return 1111;
            }
        });
    }



}
