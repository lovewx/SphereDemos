package com.multiThread;


import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * ## ThreadPoolExecutor的7个核心构造参数说明：
 * <p>
 * 参考：https://blog.csdn.net/java001122/article/details/80390771  ||   https://www.jianshu.com/p/7f828ffa6702
 * <p>
 * 1. int corePoolSize,     核心线程数：阻塞队列未满时最大工作线程数，它会一直存活，若设置allowCoreThreadTimeout=true（默认false）时，核心线程会超时关闭
 * <p>
 * 2. int maximumPoolSize,  阻塞队列满后最大工作线程数
 * <p>
 * 3. long keepAliveTime,   空闲线程存活时间；corePoolSize线程不受此参数影响
 * <p>
 * 4. TimeUnit unit,        存活时间单位
 * <p>
 * 5. BlockingQueue<Runnable> workQueue, 阻塞队列：
 * ArrayBlockingQueue：有界，FIFO，需要指定队列大小，满了会触发RejectedExecutionHandler
 * LinkedBlockingQueue    ：无界，FIFO，可以无限向队列中添加任务，直到内存溢出
 * SynchronousQueue：一种同步阻塞队列，其中每个 put 必须等待一个 take，反之亦然。可以简单理解为是一个容量只有1的队列
 * PriorityBlockingQueue： 优先级队列，线程池会优先选取优先级高的任务执行，队列中的元素必须实现Comparable接口
 * <p>
 * 6. ThreadFactory threadFactory,       创建线程工厂，如：Executors.defaultThreadFactory(),它创建的都是优化级为NORMAL的线程
 * <p>
 * 7. RejectedExecutionHandler handler   拒绝策略(下面5种)：
 * AbortPolicy：线程池队列满了丢掉这个任务并且抛出RejectedExecutionException异常。该策略是线程池的默认策略。
 * DiscardPolicy：线程池队列满了，会直接丢掉这个任务并且不会有任何异常。
 * DiscardOldestPolicy：丢弃最老的，如果失败则按该策略不断重试
 * CallerRunsPolicy：如果添加到线程池失败，那么主线程会自己去执行该任务，不会等待线程池中的线程去执行。
 * 自定义：实现RejectedExecutionHandler接口
 * <p>
 * <p>
 * ## 参数设置依据：
 *
 * ### corePoolSize：
 * 1. 计算密集型：CPU数量 + 1
 * 2. 阻塞型：W是阻塞时间，C是计算时间：
 * 当 W/C > 5 时：5*CPU数量，
 * 当 W/C < 5 时：CPU数量 * CPU利用率 * (1 + W/C)
 *
 * ## maximunPoolSize：
 */
public class ThreadPoolExecutorTest {

    private static int corePoolSize = 2;
    private static int maximunPoolSize = 2;
    private static int keepAliveTime = 2;
    private static ThreadPoolExecutor
            threadPool = new ThreadPoolExecutor(corePoolSize, maximunPoolSize, keepAliveTime, TimeUnit.SECONDS,
            new ArrayBlockingQueue<>(2), Executors.defaultThreadFactory(),
            new ThreadPoolExecutor.AbortPolicy());


}