package com.shardings.entity;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class Order {
    private Long orderId;

    private String rsoNo;

    private BigDecimal amount;

    private Long currencyId;
}
