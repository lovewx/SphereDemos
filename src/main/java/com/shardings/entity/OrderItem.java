package com.shardings.entity;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class OrderItem {
    private Long orderItemId;
    private Long orderId;
    private Long productId;
    private BigDecimal amount;
    private BigDecimal purchaseQty;
    private BigDecimal returnQty;
    private BigDecimal returnAmount;

}
