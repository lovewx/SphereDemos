package com.shardings.service;

import com.shardings.entity.Order;

import java.util.List;

public interface OrderService {

    void save(Order order);

    List<Order> getOrderList(Order order);


}
