package com.shardings.controller;

import com.shardings.entity.Order;
import com.shardings.service.OrderService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;

@Controller
@RequestMapping("/order/")
public class OrderController {
    @Resource
    private OrderService orderService;

    @RequestMapping("test")
    @ResponseBody
    public String test(){
        return "hello world";
    }

    @RequestMapping("add")
    @ResponseBody
    public String addOrder(Order order){
        try {
            orderService.save(order);
        }catch (Exception e){
            e.printStackTrace();
            return e.getMessage();
        }
        return "success";
    }

    @RequestMapping("list")
    @ResponseBody
    public Object list(Order order){
        try {
            return orderService.getOrderList(order);
        }catch (Exception e){
            e.printStackTrace();
            return e.getMessage();
        }
    }

}
