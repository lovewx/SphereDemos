//package com.shardings.sharding.shardingAlgorithm;
//
//import io.shardingsphere.api.algorithm.sharding.ListShardingValue;
//import io.shardingsphere.api.algorithm.sharding.ShardingValue;
//import io.shardingsphere.api.algorithm.sharding.complex.ComplexKeysShardingAlgorithm;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//
//import java.util.ArrayList;
//import java.util.Collection;
//import java.util.Iterator;
//import java.util.List;
//
///**
// * 自定义复合分片算法
// * 用于处理使用多键作为分片键进行分片的场景
// * <p>
// * 目前提供5种分片策略：
// * <p>
// * 标准分片策略
// * 对应StandardShardingStrategy。
// * 提供对SQL语句中的=, IN和BETWEEN AND的分片操作支持。
// * StandardShardingStrategy只支持单分片键，提供PreciseShardingAlgorithm和RangeShardingAlgorithm两个分片算法。
// * PreciseShardingAlgorithm是必选的，用于处理=和IN的分片。
// * RangeShardingAlgorithm是可选的，用于处理BETWEEN AND分片，如果不配置RangeShardingAlgorithm，SQL中的BETWEEN AND将按照全库路由处理。
// * <p>
// * 复合分片策略
// * 对应ComplexShardingStrategy。复合分片策略。
// * 提供对SQL语句中的=, IN和BETWEEN AND的分片操作支持。
// * ComplexShardingStrategy支持多分片键，由于多分片键之间的关系复杂，因此并未进行过多的封装，
// * 而是直接将分片键值组合以及分片操作符透传至分片算法，完全由应用开发者实现，提供最大的灵活度。
// * <p>
// * 行表达式分片策略
// * 对应InlineShardingStrategy。
// * 使用Groovy的表达式，提供对SQL语句中的=和IN的分片操作支持，只支持单分片键。对于简单的分片算法，可以通过简单的配置使用，
// * 从而避免繁琐的Java代码开发，如: t_user_$->{u_id % 8} 表示t_user表根据u_id模8，而分成8张表，表名称为t_user_0到t_user_7。
// * <p>
// * Hint分片策略
// * 对应HintShardingStrategy。通过Hint而非SQL解析的方式分片的策略。
// * <p>
// * 不分片策略
// * 对应NoneShardingStrategy。不分片的策略。
// */
//public class OrderComplexShardingAlgorithm implements ComplexKeysShardingAlgorithm {
//    private final static Logger log = LoggerFactory.getLogger("ShardingSphere-SQL");
//
//    /**
//     * @param availableTargetNames 可用的数据源或表名
//     * @param shardingValues 多个分片键对应的值
//     * @return 这里返回的应该是表名集合
//     */
//    /**
//     * Sharding.
//     *
//     * @param availableTargetNames available data sources or tables's names
//     * @param shardingValues sharding values
//     * @return sharding results for data sources or tables's names
//     */
//    @Override
//    public Collection<String> doSharding(Collection<String> availableTargetNames, Collection<ShardingValue> shardingValues) {
//        // rso_no分片列对应的值
//        Collection<String> rso_nos = getShardingValue(shardingValues, "rso_no");
//        List<String> shardingSuffix = new ArrayList<>();
//        for (String userIdVal : rso_nos) {
//            String key = userIdVal.replaceAll("SO", "");
//            Long result = Long.parseLong(key);
//            String suffix = "" + (result % 2);
//            availableTargetNames.forEach(x -> {
//                if (x.endsWith(suffix)) {
//                    // 得到一个订单号对应的逻辑表名
//                    shardingSuffix.add(x);
//                }
//            });
//        }
//        return shardingSuffix;
//    }
//
//    private Collection<String> getShardingValue(Collection<ShardingValue> shardingValues, final String key) {
//        Collection<String> valueSet = new ArrayList<>();
//        Iterator<ShardingValue> iterator = shardingValues.iterator();
//        while (iterator.hasNext()) {
//            ShardingValue next = iterator.next();
//            if (next instanceof ListShardingValue) {
//                ListShardingValue value = (ListShardingValue) next;
//                if (value.getColumnName().equals(key)) {
//                    return value.getValues();
//                }
//            }
//        }
//        return valueSet;
//    }
//}
