package com.shardings.sharding;//package com.sharding.sharding;
//
//import com.daling.common.dsutils.DataSourceManager;
//import com.daling.scm.service.sr.impl.validate.SpringContextHolder;
//import io.shardingsphere.core.routing.router.holder.ShardingManager;
//import io.shardingsphere.core.routing.router.masterslave.SlaveVisitedManager;
//import io.shardingsphere.shardingjdbc.jdbc.core.datasource.ShardingDataSource;
//import io.shardingsphere.shardingjdbc.jdbc.core.datasource.holder.OriginDataSourceConnectionManager;
//
//import javax.sql.DataSource;
//import java.util.Map;
//import java.util.concurrent.ConcurrentHashMap;
//
///**
// * sharding privider
// *
// * @author jys
// *
// */
//public class ShardingPrivider {
//	public static ShardingPrivider privider = new ShardingPrivider();
//	private Map<String, DataSource> dataMap = new ConcurrentHashMap<String, DataSource>();
//	private final String SHARD_PREX = "sharding_";
//
//	private ShardingPrivider() {
//	}
//
//	public Long getKey() {
//		return ShardingKeyGenerator.getInstance().getKey();
//	}
//
//	public static ShardingPrivider getInstance() {
//		return privider;
//	}
//
//	/**
//	 * set original dataSource
//	 *
//	 * @return
//	 */
//	public OriginDataSourceConnectionManager originalDataSource() {
//		return OriginDataSourceConnectionManager.getInstance().original();
//	}
//
//	public void close(OriginDataSourceConnectionManager manager) {
//		try {
//			manager.close();
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//	}
//
//	/**
//	 * set sharding tag
//	 *
//	 * @return
//	 */
//	public ShardingManager sharding() {
//		return ShardingManager.getInstance().sharding();
//	}
//
//	public SlaveVisitedManager slave() {
//		return SlaveVisitedManager.getIntance().setSlaveVisited();
//	}
//
//	public void close(ShardingManager manager) {
//		if (manager != null)
//			try {
//				manager.close();
//			} catch (Exception e) {
//				e.printStackTrace();
//			}
//	}
//
//	public ShardingManager shardingAutoClose() {
//		return ShardingManager.getInstance().shardingAutoClose();
//	}
//
//	private DataSource getDataSource(String key) {
//		DataSource d = dataMap.get(key);
//		if (d == null) {
//			Object obj=null;
//			try {
//				obj = SpringContextHolder.getBean(SHARD_PREX.concat(key));
//			} catch (Exception e) {
//			}
//			if (obj != null && obj instanceof ShardingDataSource) {
//				dataMap.put(key, (ShardingDataSource) obj);
//				return (ShardingDataSource) obj;
//			} else {
//				DataSource dataSource = DataSourceManager.getInstance().getDataSource(key);
//				dataMap.put(key, dataSource);
//				return dataSource;
//			}
//		}
//		return d;
//	}
//
//	public DataSource getShardingDataSource(String key) {
//		return getDataSource(key);
//	}
//}
