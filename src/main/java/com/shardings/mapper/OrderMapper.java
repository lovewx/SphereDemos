package com.shardings.mapper;

import com.shardings.entity.Order;
import com.shardings.entity.OrderItem;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

public interface OrderMapper {

    List<Order> getOrderList(Order order);

    Order getOrder(Long id);

    void saveOrder(Order order);

    void saveOrderItem(OrderItem orderItem);
}
