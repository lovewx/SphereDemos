package com.zk.client;

import java.io.Closeable;
import java.util.Map;

/**
 * 
 * @author haitao.wang
 *
 */
public interface ZkConfig extends Closeable {

	/**
	 * close the ZKConfig
	 */
	public void close();

	/**
	 * ZKClient获取一个Properties值
	 * 
	 */
	public String getValue(String key) throws Exception;

	public String getValue(String key, String defaultValue) throws Exception;

	/**
	 * 遍历ZKCache的所有键值
	 * 
	 * @return Map 所有键值对
	 * @throws Exception
	 */
	public Map<String, String> listAll() throws Exception;
	/**
	 * 注册Cache的Listener, 当指定路径的子节点数据有变化的时候,会有事件发生
	 * 
	 * @param cache
	 */
	// public void addListener(PathChildrenCacheListener
	// pathChildrenCacheListener);

}
