package com.zk.client;

import com.zk.ZkClientHelper;
import org.apache.curator.utils.ZKPaths;


public class ZkConfigConstant {
    public static final String PREFIX_MID_PATH = "hotconfig";
    public static final String CFG_PATH = ZKPaths.makePath(ZkClientHelper.BASE_PATH, PREFIX_MID_PATH);

}
