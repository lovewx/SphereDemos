package com.zk.client;

import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.recipes.cache.PathChildrenCacheEvent;
import org.apache.curator.framework.recipes.cache.PathChildrenCacheListener;

public interface ZkConfigListener extends PathChildrenCacheListener {

    /**
     * 当指定路径的子节点数据有变化的时候,会有事件发生
     *
     * @param client the client
     * @param event describes the change
     * @throws Exception errors
     */
    public void childEvent(CuratorFramework client, PathChildrenCacheEvent event) throws Exception;

}
