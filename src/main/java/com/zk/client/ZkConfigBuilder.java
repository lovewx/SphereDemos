package com.zk.client;

import com.google.common.base.Preconditions;
import com.zk.client.impl.ZkConfigImpl;
import org.apache.commons.lang3.StringUtils;
import org.apache.curator.utils.ZKPaths;

/**
 * 
 * @author haitao.wang
 * 
 */
public class ZkConfigBuilder {
    //private static final Logger log = LoggerFactory.getLogger(ZKConfigBuilder.class);
    private static final boolean DEFAULT_IFCACHEDATA = true; // true缓存数据到本地,可直接获取; false则只用于事件感知后,再从远端获取数据

    /**
     * 私有构造函数
     */
    private ZkConfigBuilder() {
    }

    public static ZkConfigBuilder newBuilder() {
        return new ZkConfigBuilder();
    }

    private boolean ifCacheData = DEFAULT_IFCACHEDATA;
    private String prefixFullPath = ZkConfigConstant.CFG_PATH;
    private String appPath = "";
    private String groupPath = "";
    private String configFullPath = "";
    private ZkConfigListener zkConfigListener;
    /**
     * 用当前值构造ZKConfig
	 * @return ZKConfig
     */
    public ZkConfig build() {
        Preconditions.checkState(!StringUtils.isBlank(this.appPath), "appPath should not be empty");
        if(!StringUtils.isBlank(this.groupPath)){
            this.configFullPath = ZKPaths.makePath(this.appPath, this.groupPath);
        }else{
            this.configFullPath = this.appPath;
        }
        //log.debug("this.nodeFullPath={}",this.configFullPath);
        return new ZkConfigImpl(this);
    }

    /**
	 * @return ZKConfig
     */
    public ZkConfigBuilder setIfCacheData(boolean ifCacheData) {
        this.ifCacheData = ifCacheData;
        return this;
    }

    /**
     */
    public boolean getIfCacheData() {
        return this.ifCacheData;
    }

    /**
     */
    public String getAppPath() {
        return this.appPath;
    }

    /**
     */
    public ZkConfigBuilder setAppPath(String thisAppPath) {
        Preconditions.checkNotNull(thisAppPath, "appPath should not be null");
        Preconditions.checkArgument(!thisAppPath.contains("/"), "Invalid group name, could not contains '/', " + thisAppPath);
        this.appPath = thisAppPath;
        return this;
    }

    /**
     */
    public ZkConfigListener getZKConfigListener() {
        return this.zkConfigListener;
    }

    /**
     */
    public ZkConfigBuilder registerZKConfigListener(ZkConfigListener thisZKConfigListener) {
        Preconditions.checkNotNull(thisZKConfigListener, "ZKConfigListener should not be null");        
        this.zkConfigListener = thisZKConfigListener;
        return this;

    }

    /**
     */
    public String getGroupPath() {
        return this.groupPath;
    }

    /**
     */
    public ZkConfigBuilder appendGroupPath(String thisGroupPath) {
        Preconditions.checkNotNull(thisGroupPath, "groupPath should not be null");
        Preconditions.checkArgument(!thisGroupPath.contains("/"), "Invalid group name, could not contains '/', "
                + thisGroupPath);
        if(StringUtils.isBlank(this.groupPath)){
            this.groupPath = thisGroupPath;
        }else{
            this.groupPath = ZKPaths.makePath(this.groupPath, thisGroupPath);
        }

        return this;

    }
    
    /**
     */
    public String getPrefixFullPath() {
        return this.prefixFullPath;
    }
    
    /**
     */
    public String getConfigFullPath() {
        return this.configFullPath;
    }

}
