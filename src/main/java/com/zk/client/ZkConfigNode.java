package com.zk.client;

import java.util.List;

public class ZkConfigNode {
    private String fullPath;
    private String nodeName;
    private String nodeValue;
    private List<ZkConfigNode> childList;
    
    public String getFullPath() {
        return fullPath;
    }
    public void setFullPath(String fullPath) {
        this.fullPath = fullPath;
    }
    public String getNodeName() {
        return nodeName;
    }
    public void setNodeName(String nodeName) {
        this.nodeName = nodeName;
    }
    public String getNodeValue() {
        return nodeValue;
    }
    public void setNodeValue(String nodeValue) {
        this.nodeValue = nodeValue;
    }
    public List<ZkConfigNode> getChildList() {
        return childList;
    }
    public void setChildList(List<ZkConfigNode> childList) {
        this.childList = childList;
    }
    @Override
    public String toString() {
        return "ZKConfigNode [fullPath=" + fullPath + ", nodeName=" + nodeName + ", nodeValue=" + nodeValue
                + ", childList=" + childList + "]";
    }
    
    
}
