package com.zk.client.impl;

import com.zk.client.ZkConfigListener;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.recipes.cache.PathChildrenCacheEvent;
import org.apache.curator.utils.ZKPaths;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ZkConfigListenerImplSample implements ZkConfigListener {
    private final Logger log = LoggerFactory.getLogger(this.getClass());
    private String name;

    public ZkConfigListenerImplSample(){
    }
    
    public ZkConfigListenerImplSample(String name){
        this.name = name;
    }
    @Override
    public void childEvent(CuratorFramework client, PathChildrenCacheEvent event) throws Exception {
        PathChildrenCacheEvent.Type eventType = event.getType();
        String fullpath = null;
        String path = null;
        String data = null;
        switch (eventType)
        {
        case CHILD_ADDED: {
            fullpath = event.getData().getPath();
            path = ZKPaths.getNodeFromPath(event.getData().getPath());
            data = new String(event.getData().getData(), "UTF-8");
            log.debug("Name=[{}],Event=[{}],FullPath=[{}],Path=[{}],Data=[{}]", name, eventType, fullpath, path, data);
            break;
        }
        
        case CHILD_UPDATED: {
            fullpath = event.getData().getPath();
            path = ZKPaths.getNodeFromPath(event.getData().getPath());
            data = new String(event.getData().getData(), "UTF-8");
            log.debug("Name=[{}],Event=[{}],FullPath=[{}],Path=[{}],Data=[{}]", name, eventType, fullpath, path, data);
            break;
        }
        case CHILD_REMOVED: {
            fullpath = event.getData().getPath();
            path = ZKPaths.getNodeFromPath(event.getData().getPath());
            data = new String(event.getData().getData(), "UTF-8");
            log.debug("Name=[{}],Event=[{}],FullPath=[{}],Path=[{}],Data=[{}]", name, eventType, fullpath, path, data);
            break;
        }
        case CONNECTION_SUSPENDED: {
            log.debug("Name=[{}],Event=[{}]", name, eventType);
            break;
        }
        case CONNECTION_RECONNECTED: {
            log.debug("Name=[{}],Event=[{}]", name, eventType);
            break;
        }
        case CONNECTION_LOST: {
            log.debug("Name=[{}],Event=[{}]", name, eventType);
            break;
        }
        case INITIALIZED: {
            log.debug("Name=[{}],Event=[{}]", name, eventType);
            break;
        }
        }
    }

}
