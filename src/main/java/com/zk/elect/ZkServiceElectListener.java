package com.zk.elect;

import org.apache.curator.framework.recipes.leader.LeaderLatchListener;

public interface ZkServiceElectListener extends LeaderLatchListener {

    public void isLeader();


    public void notLeader();

}
