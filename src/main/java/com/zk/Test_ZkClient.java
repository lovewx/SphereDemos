package com.zk;

import org.I0Itec.zkclient.IZkDataListener;
import org.I0Itec.zkclient.ZkClient;

import java.util.List;

/**
 * zookeeper客户端测试
 */
public class Test_ZkClient {

    public static void main(String[] args) {
        ZkClient zkClient = new ZkClient("localhost:2181");
        // 设置序列
        zkClient.setZkSerializer(new ZkMySerializer());
        // 监听peter节点，也可以监听子节点如：peter/child
        zkClient.subscribeDataChanges("/peter", new IZkDataListener() {
            @Override
            public void handleDataChange(String s, Object o) throws Exception {
                System.out.println("监听到数据节点被删除了");
            }

            @Override
            public void handleDataDeleted(String s) throws Exception {
                System.out.println("监听到数据改变了，新值为："+s);
            }
        });
        if(!zkClient.exists("/pp")){
            System.out.println("新建");
            zkClient.createPersistent("/pp");
        }

        if(zkClient.exists("/pp")){
            System.out.println("存在");
        }
        List<String> list = zkClient.getChildren("/peter");
        for(String child: list){
            System.out.println(child);
        }
    }

}
