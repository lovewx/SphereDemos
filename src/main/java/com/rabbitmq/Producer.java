package com.rabbitmq;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class Producer {
    static Logger log = LoggerFactory.getLogger(Producer.class);

    public static void main(String[] args) throws IOException, TimeoutException {
        ConnectionFactory factory = new ConnectionFactory();
        //新增代码，如果连接断开会自动重连
        //factory.setAutomaticRecoveryEnabled(true);
        factory.setHost("127.0.0.1");
        factory.setPort(5672);
        //factory.setVirtualHost(vhost);
        factory.setUsername("peter");
        factory.setPassword("123456");
        factory.setVirtualHost("/");
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();

//        channel.exchangeDeclare("hawk_pretreatment_exchange_test", "fanout",false);
//        channel.queueDeclare(QUEUE_NAME, false, false, false, null);
        channel.queueBind("erp_receive_hawk_pretreatment_queue_test", "hawk_pretreatment_exchange_test", "#");//#号接收所有的数据
        channel.basicPublish("hawk_pretreatment_exchange_test", "#", null, "peter".getBytes());


    }
}
