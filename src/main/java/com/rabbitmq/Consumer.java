package com.rabbitmq;

import com.rabbitmq.client.*;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class Consumer {

    public static void main(String[] args) throws IOException, TimeoutException {
        ConnectionFactory factory = new ConnectionFactory();
        //新增代码，如果连接断开会自动重连
        //factory.setAutomaticRecoveryEnabled(true);
        factory.setHost("127.0.0.1");
        factory.setPort(5672);
        //factory.setVirtualHost(vhost);
        factory.setUsername("peter");
        factory.setPassword("123456");
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();
        channel.exchangeDeclare("hawk_pretreatment_exchange_test", "direct",true);
//        channel.queueDeclare(QUEUE_NAME, false, false, false, null);
        com.rabbitmq.client.Consumer consumers = new DefaultConsumer(channel) {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                String message = new String(body, "UTF-8");
                System.out.println("Customer Received '" + message + "'");
            }
        };
        //具体的业务逻辑在CustomerMqConsumer中
        channel.queueBind("erp_receive_hawk_pretreatment_queue_test", "hawk_pretreatment_exchange_test","#");//#号接收所有的数据
        channel.basicConsume("erp_receive_hawk_pretreatment_queue_test",true,consumers);
//channel.close();
    }
}
