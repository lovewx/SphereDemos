
 **Mycat官网**  ：http://mycat.io/

# 一、互联网架构演进

1. 能不拆分 尽量不拆分
2. 数据拆分尽量通过数据冗余或表分组来降低跨库join的可能性
3. 大部架构师认为1000万数据后可分表

# 二、 分库分表解决方案

### 应用层
1.当当：sharding-jdbc

2.阿里：TDDL

### Proxy
社区：mycat-cobar

数字：atlas

金山：kingshard

百度：heinsberge

商业版：oneproxy

youtube:vitess

### 分库分表方案
1.  **水平拆分** （横向拆分）

将不同的业务表拆分到不同的数据库中
 **
2. ** 垂直拆分（纵向拆分）

将同一表拆分到不同的数据库中


# 三、mycat分库分表原理与实践
### 什么是mycat
1. 一个彻底开源的，面向企业应用开发的大数据库集群
2. 支持事务、ACID、可以替代mysql的加强版数据库，还可配置数据库主从
3. 一个可以视为mysql集群的企业级数据库，用来替代昂贵的oracle集群
4. 一个融合内存缓存技术、nosql技术、HDFS大数据的新型SQLserver
5. 结合传统数据库和新 型分布式数据仓库的新一代企业级数据库产品
6. 一个新颖的数据库中间件产品



### 通俗解释
mycat就是一个数据库中间件，数据库的代理，它屏蔽了物理数据库，应用连接mycat,然后mycat再连接 物理数据库

它支持水平拆分（分库分表：通过分库达到分表），支持多种分片规则，比如范围切片、自然月份切片、hash取模分片等

它支持mysql、oracle、mongodb、sqlserver,并且 支持数据库集群


### mycat特性
1. 分片规则和策略：多种分片规则策略，还可自定义
1. 分布式全局唯一ID：多种分布式全局唯一ID实现 (默认雪花算法)
1. 多数据源管理问题：统一管理所有数据源
1. 跨库跨表join问题：全局表/shared join / cat left
1. 其他特点：
- 独创ER关系分片，解决E-R分片难处理问题
- 采用全局分片技术，每个节点同时并发插入和更新数据，都可以读取数据
- 通过人工智能的catlet支持跨分片复杂SQL实现以及存储过程支持等

### mycat连接和配置
1. mycat的连接和连接mysql数据库一样，也可用navicat工具。
2. mycat软件的server.xml里可以配置连接的mysql等
2. mycat软件的schema.xml里可以配置逻辑库等
2. mycat软件的rule.xml里可以配置分片规则、策略等
2. io.mycat.route.function.PartitionByFileMap策略的mapFile(partition-hash-init.txt)可用来配置不同的hash值对应不同的库，如：
- 10000 = 0  分库字段sharding-id指定值为1000时，会落到0库，即落到第一个数据库节点
- 10010 = 1  分库字段sharding-id指定值为1010时，会落到1库，即落到第二个数据库节点
- 所以，mycat里这种需要在表里添加一个sharding-id字段

### mycat架构图
![输入图片说明](https://images.gitee.com/uploads/images/2019/0831/164257_cf3b629a_1365433.png "arc.png")


# 四、分库分表后的问题

### 水平拆分带来的问题
1. 拆分规则难以抽象
2. 分片事务一致性难以解决
3. 维护难度大
4. 跨库jOin性能差


### 垂直拆分带来的问题
1. 部分业务表无法join，只能通过接口方式，提高了系统复杂度
2. 存在单表性能瓶颈，不易扩展
3. 事务处理复杂 

# 五、使用场景








