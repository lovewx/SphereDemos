package com.distribute.transaction;

import com.rabbitmq.client.Channel;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.support.AmqpHeaders;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.sql.SQLException;

/**
 * 订单分发
 * <p>
 * 如果MQ中unacked大于0，则说明消费者有问题：
 * 可能是数据消费成功了，但没有告诉MQ，好了之后，就有可能会重复消费
 * 也有可能是完全不能消费消息了
 */
@Component
public class OrderDispatchConsumer {

    @RabbitListener(queues = "orderDispatchQueue")
    public void messageConsumer(String message, Channel channel, @Header(AmqpHeaders.DELIVERY_TAG) long tag) throws IOException {
        try {

            System.out.println(message);
            // 分发业务
            throw new SQLException("业务处理SQl异常");

        } catch (SQLException e) {
            // 这类异常能处理，告诉队列不需要重发了
            channel.basicNack(tag, false, false);
        } catch (Exception e) {
            // 告诉队列再发一次
            channel.basicNack(tag, false, true);
        }
    }
}
