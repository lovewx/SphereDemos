package com.lock.zklock;

import com.lock.ZLock;
import com.zk.ZkClientHelper;

public class LockHelper {

	public static ZLock getZLock(String lockName) {
		assistCheckNotNull(lockName, "lockName should not be null");
		return new ZKLock(ZkClientHelper.getZkClient(), lockName);
	}

	public static ZLock getReentrantZLock(String lockName) {
		assistCheckNotNull(lockName, "lockName should not be null");
		return new ZKReentrantLock(ZkClientHelper.getZkClient(), lockName);
	}

//	public static RLock getRLock(String lockName) {
//		assistCheckNotNull(lockName, "lockName should not be null");
//		return new RedisLock(ZkClientHelper.getZkClient(), lockName);
//	}

//	public static RLock getReentrantRLock(String lockName) {
//		assistCheckNotNull(lockName, "lockName should not be null");
//		return new ReentrantRedisLock(LockUtilsConfig.getRedisCmd(), lockName);
//	}

	private static <T> T assistCheckNotNull(T reference, Object errorMessage) {
		if (reference == null) {
			throw new NullPointerException(String.valueOf(errorMessage));
		}
		return reference;
	}
}
