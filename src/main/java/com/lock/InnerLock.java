package com.lock;

import java.io.Closeable;
import java.time.Duration;
import java.util.concurrent.TimeUnit;

interface InnerLock extends Closeable {
	/**
	 * 获取锁, 等待直到返回
	 */
	void lock() throws Exception;

	/**
	 * 尝试获取锁, 未指定过期自动释放时间, 使用默认时间60秒
	 */
	boolean tryLock() throws Exception;

	/**
	 * 尝试获取锁, 指定过期自动释放时间
	 */
	boolean tryLock(long waitTime, TimeUnit unit) throws Exception;

	boolean tryLock(Duration waitTime) throws Exception;

	void unlock() throws Exception;

	boolean isLocked();

	boolean isHoldByCurrentThread();

	int getHoldCount();

	void close();
}
