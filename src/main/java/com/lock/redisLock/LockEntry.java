package com.lock.redisLock;

public class LockEntry {
    String key = null;
    String uuid = null;
    String thd = null;
    Long cnt = 0L;
    Long leasetime = 0L;

/*
public LockEntry(){
}
*/

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public Long getCnt() {
        return cnt;
    }

    public void setCnt(Long cnt) {
        this.cnt = cnt;
    }

    public String getThd() {
        return thd;
    }

    public void setThd(String thd) {
        this.thd = thd;
    }

    public Long getLeasetime() {
        return leasetime;
    }

    public void setLeasetime(Long leasetime) {
        this.leasetime = leasetime;
    }

    @Override
    public String toString() {
        return "LockEntry [key=" + key + ", uuid=" + uuid + ", thd=" + thd + ", cnt=" + cnt + ", leasetime=" + leasetime + "]";
    }
}