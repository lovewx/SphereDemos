package com.lock.redisLock;

import com.google.common.collect.Lists;

import java.util.List;

public class Test_redisLock {

    public static void main(String[] args) throws Exception {
        // 单个锁
        try {
            boolean b = RedisLockHelpers.getRLock("testLocak").tryLock();
            if (b) {
                System.out.println("线程1获得锁：" + System.currentTimeMillis());
            } else {
                System.out.println("线程1未获得锁：" + System.currentTimeMillis());
            }
            boolean bb = RedisLockHelpers.getRLock("testLocak").tryLock();
            if (bb) {
                System.out.println("线程2获得锁：" + System.currentTimeMillis());
            } else {
                System.out.println("线程2未获得锁：" + System.currentTimeMillis());
            }
        } finally {
            RedisLockHelpers.getRLock("testLocak").unlock();
        }
        try {
            boolean bb = RedisLockHelpers.getRLock("testLocak").tryLock();
            if (bb) {
                System.out.println("线程3获得锁：" + System.currentTimeMillis());
            } else {
                System.out.println("线程3未获得锁：" + System.currentTimeMillis());
            }
        } finally {
            RedisLockHelpers.getRLock("testLocak").unlock();
        }
        // 批量锁
        RSOLockUtil rbLock = RSOLockUtil.create();
        try {
            List<String> list = Lists.newArrayList("pnCode1", "pnCode2");
            rbLock.setProdCode(list.toArray(new String[list.size()])).lock();
        } finally {
            rbLock.unlock();//JDK8可以放在try里释放，因为实现了Closeable
        }

    }


}
