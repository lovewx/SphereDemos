package com.lock.redisLock;

import com.lock.RLock;
import com.lock.util.LockUtilsConfig;

/**
 * @author 007
 */
public class RedisLockHelpers {


    public static RLock getRLock(String lockName) {
        assistCheckNotNull(lockName, "lockName should not be null");
        // 非重入
        return new RedisLock(LockUtilsConfig.getRedisCmd(), lockName);
    }

//	public static RLock getReentrantRLock(String lockName) {
//		assistCheckNotNull(lockName, "lockName should not be null");
//		return new ReentrantRedisLock(LockUtilsConfig.getRedisCmd(), lockName);
//	}

    private static <T> T assistCheckNotNull(T reference, Object errorMessage) {
        if (reference == null) {
            throw new NullPointerException(String.valueOf(errorMessage));
        }
        return reference;
    }
}
