package com.lock.redisLocks.ledixbinary;

import java.io.Closeable;

import com.lock.redisLocks.RedisHelper.ProviderConfigType;
import com.lock.redisLocks.RedisHelper.RedisConfigType;

import io.lettuce.core.cluster.api.sync.RedisClusterCommands;

public interface LedixBinary extends RedisClusterCommands<byte[], byte[]>, Closeable{

	RedisConfigType getRedisConfigType();

	ProviderConfigType getProviderConfigType();

	<T> T getOriginCommands();
	
	@Override
	public void close();
}
