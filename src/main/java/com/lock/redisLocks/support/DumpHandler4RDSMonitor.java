package com.lock.redisLocks.support;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

//import com.daling.common.dmonitor.handler.DumpDataHandler;

public class DumpHandler4RDSMonitor {

    public Map<String, String> dump() throws Exception {
        Map<String, String> items = new HashMap<>();
        //数据库连接池监控数据
        for (Entry<String, Long> entry : RDSMonitor.getValues().entrySet()) {
            String name = entry.getKey();
            Long value = entry.getValue();
            items.put(name, value.toString());
        }
        return items;
    }


}
