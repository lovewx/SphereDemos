package com.lock.redisLocks.support;

/** <p>Title: TwoTuple</p>
 * <p>Description: 两个元素的元组，用于在一个方法里返回两种类型的值</p>
 * @author Xewee.Zhiwei.Wang@gmail.com
 * @site http://wzwahl36.net
 * @version 2012-3-21 上午11:15:03
 * @param <A>
 * @param <B>
 */
public class TwoTuple<A, B> {
    private final A o1;
    private final B o2;
     
    public TwoTuple(A a, B b) {
        this.o1 = a;
        this.o2 = b;
    }
    public A getItem1() {
    	return o1;
    }
    public B getItem2() {
    	return o2;
    }
}