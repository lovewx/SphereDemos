package com.lock.redisLocks.ledix;

import java.io.Closeable;

import com.lock.redisLocks.RedisHelper.ProviderConfigType;
import com.lock.redisLocks.RedisHelper.RedisConfigType;

import io.lettuce.core.cluster.api.sync.RedisClusterCommands;

public interface Ledix extends RedisClusterCommands<String, String>, Closeable {

	RedisConfigType getRedisConfigType();

	ProviderConfigType getProviderConfigType();

	<T> T getOriginCommands();

	@Override
	public void close();
}
