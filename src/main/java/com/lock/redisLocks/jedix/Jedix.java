package com.lock.redisLocks.jedix;

import com.lock.redisLocks.RedisHelper;

import java.io.Closeable;


public interface Jedix extends IJedixCommands, IJedixBasicCommands, IJedixMultiKeyCommands, Closeable {

	RedisHelper.RedisConfigType getRedisConfigType();

	RedisHelper.ProviderConfigType getProviderConfigType();

	<T> T getOriginImpl();

	@Override
	public void close();
}
