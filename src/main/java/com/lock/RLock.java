package com.lock;

import java.time.Duration;
import java.util.concurrent.TimeUnit;

public interface RLock extends InnerLock {
    /**
     * @param waitTime
     * @param waitUnit
     * @param leaseTime
     * @param leaseUnit
     * @return
     * @throws Exception
     */
    boolean tryLock(long waitTime, TimeUnit waitUnit, long leaseTime, TimeUnit leaseUnit) throws Exception;

    /**
     * @param waitTime
     * @param leaseTime
     * @return
     * @throws Exception
     */
    boolean tryLock(Duration waitTime, Duration leaseTime) throws Exception;
}